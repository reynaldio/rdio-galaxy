package model

type PhoneNumber struct {
	PhoneCountryId   string `bson:"phone_country_id" json:"phone_country_id"`
	PhoneCountryCode int32  `bson:"phone_country_code" json:"phone_country_code"`
	PhoneNumber      string `bson:"phone_number" json:"phone_number"`
	FullPhoneNumber  string `bson:"full_phone_number" json:"full_phone_number"`
}
