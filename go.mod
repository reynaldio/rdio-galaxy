module gitlab.com/reynaldio/rdio-galaxy

go 1.20

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/google/uuid v1.6.0
	github.com/jackc/pgx-gofrs-uuid v0.0.0-20230224015001-1d428863c2e2
	github.com/jackc/pgx/v5 v5.5.2
	github.com/nicksnyder/go-i18n/v2 v2.3.0
	github.com/nyaruka/phonenumbers v1.3.1
	go.uber.org/zap v1.26.0
	golang.org/x/text v0.14.0
)

require (
	github.com/gofrs/uuid/v5 v5.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
