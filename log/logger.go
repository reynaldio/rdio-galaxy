package log

import (
	"go.uber.org/zap"
)

type Logger interface {
	Debug(msg string, keysAndValues ...interface{})
	Info(msg string, keysAndValues ...interface{})
	Warn(msg string, keysAndValues ...interface{})
	Error(msg string, keysAndValues ...interface{})
	Panic(msg string, keysAndValues ...interface{})
	Fatal(msg string, keysAndValues ...interface{})

	DebugE(err error, keysAndValues ...interface{})
	InfoE(err error, keysAndValues ...interface{})
	WarnE(err error, keysAndValues ...interface{})
	ErrorE(err error, keysAndValues ...interface{})
	PanicE(err error, keysAndValues ...interface{})
	FatalE(err error, keysAndValues ...interface{})
}

func NewLogger(isProduction bool) Logger {
	var zapLogger *zap.Logger
	if isProduction {
		zapLogger = zap.Must(zap.NewProduction())
	} else {
		zapLogger = zap.Must(zap.NewDevelopment())
	}

	defer zapLogger.Sync() // flushes buffer, if any

	instance := &logger{
		plainLogger:   zapLogger,
		sugaredLogger: zapLogger.Sugar(),
	}

	return instance
}

type logger struct {
	plainLogger   *zap.Logger
	sugaredLogger *zap.SugaredLogger
}

func (self *logger) Debug(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Debugw(msg, keysAndValues...)
}

func (self *logger) Info(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Infow(msg, keysAndValues...)
}

func (self *logger) Warn(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Warnw(msg, keysAndValues...)
}

func (self *logger) Error(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Errorw(msg, keysAndValues...)
}

func (self *logger) Panic(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Panicw(msg, keysAndValues...)
}

func (self *logger) Fatal(msg string, keysAndValues ...interface{}) {
	self.sugaredLogger.Fatalw(msg, keysAndValues...)
}

func joinErrorWithKeyValues(err error, keysAndValues ...interface{}) []interface{} {
	keyVals := make([]interface{}, 0, len(keysAndValues)+1)
	keyVals = append(keyVals, "error", err)
	for _, v := range keysAndValues {
		keyVals = append(keyVals, v)
	}

	return keyVals
}

func (self *logger) DebugE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Debugw(err.Error(), keyVals...)
}

func (self *logger) InfoE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Infow(err.Error(), keyVals...)
}

func (self *logger) WarnE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Warnw(err.Error(), keyVals...)
}

func (self *logger) ErrorE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Errorw(err.Error(), keyVals...)
}

func (self *logger) PanicE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Panicw(err.Error(), keyVals...)
}

func (self *logger) FatalE(err error, keysAndValues ...interface{}) {
	keyVals := joinErrorWithKeyValues(err, keysAndValues...)
	self.sugaredLogger.Fatalw(err.Error(), keyVals...)
}
