package localization

import (
	"encoding/json"

	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
)

type Localizer interface {
	Init(langs map[string]string) error
	Localize(lang string, messageId string, messageParams interface{}) string
}

func NewLocalizer() Localizer {
	instance := &localizer{
		localizerInstances: map[string]*i18n.Localizer{},
	}

	return instance
}

type localizer struct {
	bundle             *i18n.Bundle
	localizerInstances map[string]*i18n.Localizer
}

func (self *localizer) Init(langs map[string]string) error {
	bundle := i18n.NewBundle(language.English)
	self.bundle = bundle

	bundle.RegisterUnmarshalFunc("json", json.Unmarshal)
	bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)

	for _, fn := range langs {
		bundle.MustLoadMessageFile(fn)
	}

	for langId := range langs {
		inst := i18n.NewLocalizer(bundle, langId)
		self.localizerInstances[langId] = inst
	}

	return nil
}

func (self *localizer) Localize(langId string, messageId string, messageParams interface{}) string {
	lid := langId
	if len(lid) <= 0 {
		lid = "en"
	}

	mValue := ""

	l := self.localizerInstances[langId]
	if l != nil {
		lValue, err := l.Localize(&i18n.LocalizeConfig{
			MessageID:    messageId,
			TemplateData: messageParams,
		})

		if err != nil {
			return messageId
		}

		mValue = lValue
	}

	if len(mValue) <= 0 {
		return messageId
	}

	return mValue
}
