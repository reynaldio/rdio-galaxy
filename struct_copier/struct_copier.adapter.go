package struct_copier

import (
	"time"
)

type StructCopierAdapterFunc func(FieldValue interface{}) (interface{}, error)

func StringToTimeAdapter(dateLayout string) StructCopierAdapterFunc {
	return func(FieldValue interface{}) (interface{}, error) {
		s := FieldValue.(string)
		if len(s) == 0 {
			return time.Time{}, nil
		}

		return time.Parse(dateLayout, FieldValue.(string))
	}
}
