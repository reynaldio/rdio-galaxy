package struct_copier

import (
	"errors"
	"fmt"
	"reflect"
)

type structCopier struct {
}

type CopyStructValueCallback func(sourceFieldName string, sourceFieldType string, sourceValue interface{}) (string, interface{}, bool, error)
type StructCopierAdapterMap map[string]StructCopierAdapterFunc

func (self *structCopier) getRef(obj interface{}, isTarget bool) (reflect.Value, error) {
	ref := reflect.ValueOf(obj)

	if isTarget {
		if ref.Kind() != reflect.Ptr && ref.Kind() != reflect.Interface {
			return ref, errors.New("Object must be of type pointer or interface of struct")
		}
	}

	// if its a pointer, resolve its value
	if ref.Kind() == reflect.Ptr {
		ref = reflect.Indirect(ref)
	}

	if ref.Kind() == reflect.Interface {
		ref = ref.Elem()
	}

	// should double check we now have a struct (could still be anything)
	if ref.Kind() != reflect.Struct {
		return ref, errors.New("Object must be of struct type")
	}

	return ref, nil
}

func (self *structCopier) CopyStructValue(source interface{}, target interface{}, callback CopyStructValueCallback, adapters StructCopierAdapterMap) error {
	sourceRef, err := self.getRef(source, false)
	if err != nil {
		return err
	}

	targetRef, err := self.getRef(target, true)
	if err != nil {
		return err
	}

	fieldCount := sourceRef.NumField()
	for i := 0; i < fieldCount; i++ {
		sourceRefField := sourceRef.Type().Field(i)
		sourceField := sourceRef.Field(i)
		sourceFieldType := sourceField.Type().String()
		sourceFieldName := sourceRefField.Name
		sourceFieldValue := sourceField.Interface()

		if callback != nil {
			targetFieldName, targetFieldValue, targetHandled, err := callback(sourceFieldName, sourceFieldType, sourceFieldValue)
			if err != nil {
				return err
			}

			if targetHandled {
				targetField := targetRef.FieldByName(targetFieldName)
				if targetField.IsZero() {
					return errors.New(fmt.Sprintf("Field %s not found", targetFieldName))
				}

				targetField.Set(reflect.ValueOf(targetFieldValue))

				return nil
			}
		}

		targetField := targetRef.FieldByName(sourceFieldName)
		if targetField.IsZero() {
			continue
		}

		var adapter StructCopierAdapterFunc
		if adapters != nil && len(adapters) > 0 {
			val, ok := adapters[sourceFieldName]
			if ok {
				adapter = val
			}
		}

		if adapter != nil {
			aValue, err := adapter(sourceFieldValue)
			if err != nil {
				return err
			}

			targetField.Set(reflect.ValueOf(aValue))
		} else {
			targetFieldType := targetField.Type().String()
			if targetFieldType != sourceFieldType {
				continue
			}

			targetField.Set(reflect.ValueOf(sourceFieldValue))
		}
	}

	return nil
}

var StructCopier = &structCopier{}
