package sql

import (
	"database/sql"
	"strings"
)

type sqlUtil struct {
}

func (self *sqlUtil) ToNullStringIfEmpty(s string) sql.NullString {
	if len(strings.TrimSpace(s)) == 0 {
		return sql.NullString{}
	}

	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

var SqlUtil = &sqlUtil{}
