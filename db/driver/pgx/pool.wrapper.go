package pgx

import (
	"context"
	"fmt"

	pgxuuid "github.com/jackc/pgx-gofrs-uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/tracelog"

	tLogger "gitlab.com/reynaldio/rdio-galaxy/log"
)

type PgxPoolWrapper interface {
	Pool() *pgxpool.Pool
	Close()
	GetConn(ctx context.Context) (PgxConnWrapper, error)
}

type pgxPoolWrapper struct {
	dbPool *pgxpool.Pool
}

func NewPgxPool(ctx context.Context, connString string, logger tLogger.Logger, isProduction bool) (PgxPoolWrapper, error) {
	conf, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, err
	}

	// ref: https://github.com/jackc/pgx/wiki/UUID-Support
	conf.AfterConnect = func(ctx context.Context, conn *pgx.Conn) error {
		pgxuuid.Register(conn.TypeMap())
		return nil
	}

	var logLevel tracelog.LogLevel
	if isProduction {
		logLevel = tracelog.LogLevelInfo
	} else {
		logLevel = tracelog.LogLevelDebug
	}

	conf.ConnConfig.Tracer = &tracelog.TraceLog{
		Logger:   &PgxLogger{Logger: logger},
		LogLevel: logLevel,
	}

	// pgxpool default max number of connections is the number of CPUs on your machine returned by runtime.NumCPU().
	// This number is very conservative, and you might be able to improve performance for highly concurrent applications
	// by increasing it.
	// conf.MaxConns = runtime.NumCPU() * 5

	pool, err := pgxpool.NewWithConfig(ctx, conf)
	if err != nil {
		return nil, fmt.Errorf("Pgx connection error: %w", err)
	}

	instance := &pgxPoolWrapper{
		dbPool: pool,
	}

	return instance, nil
}

func (self *pgxPoolWrapper) Pool() *pgxpool.Pool {
	return self.dbPool
}

func (self *pgxPoolWrapper) Close() {
	if self.dbPool != nil {
		self.dbPool.Close()
	}
}

func (self *pgxPoolWrapper) GetConn(ctx context.Context) (PgxConnWrapper, error) {
	conn, err := self.dbPool.Acquire(ctx)
	if err != nil {
		// fmt.Fprintf(os.Stderr, "Unable to acquire connection: %v\n", err)
		// os.Exit(1)
		return nil, fmt.Errorf("Unable to acquire connection: %v\n", err)
	}

	instance := &pgxConnWrapper{
		conn: conn,
	}

	return instance, nil
}
