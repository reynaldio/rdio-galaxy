package pgx

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PgxConnWrapTxFunc = func(ctx context.Context, tx PgxTxWrapper) error

type PgxConnWrapper interface {
	Conn() *pgxpool.Conn
	Release()
	DefaultTxOptions() pgx.TxOptions
	BeginTx(ctx context.Context, txOptions pgx.TxOptions) (PgxTxWrapper, error)
	WrapTx(ctx context.Context, txOptions pgx.TxOptions, wrapFunc PgxConnWrapTxFunc) error
}

type pgxConnWrapper struct {
	conn *pgxpool.Conn
}

func (self *pgxConnWrapper) Conn() *pgxpool.Conn {
	return self.conn
}

func (self *pgxConnWrapper) Release() {
	if self.conn != nil {
		self.conn.Release()
	}
}

func (self *pgxConnWrapper) DefaultTxOptions() pgx.TxOptions {
	return pgx.TxOptions{}
}

func (self *pgxConnWrapper) BeginTx(ctx context.Context, txOptions pgx.TxOptions) (PgxTxWrapper, error) {
	tx, err := self.conn.BeginTx(ctx, txOptions)
	if err != nil {
		return nil, err
	}

	instance := &pgxTxWrapper{
		conn: self,
		tx:   tx,
	}

	return instance, nil
}

func (self *pgxConnWrapper) WrapTx(ctx context.Context, txOptions pgx.TxOptions, wrapFunc PgxConnWrapTxFunc) error {
	tx, err := self.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Tx().Rollback(ctx)
		} else {
			tx.Tx().Commit(ctx)
		}
	}()

	err = wrapFunc(ctx, tx)
	return err
}
