package pgx

import (
	"github.com/jackc/pgx/v5"
)

type PgxTxWrapper interface {
	Conn() PgxConnWrapper
	Tx() pgx.Tx
}

type pgxTxWrapper struct {
	conn PgxConnWrapper
	tx   pgx.Tx
}

func (self *pgxTxWrapper) Conn() PgxConnWrapper {
	return self.conn
}

func (self *pgxTxWrapper) Tx() pgx.Tx {
	return self.tx
}
