package pgx

import (
	"context"

	ilog "gitlab.com/reynaldio/rdio-galaxy/log"

	etracelog "github.com/jackc/pgx/v5/tracelog"
)

type PgxLogger struct {
	Logger ilog.Logger
}

func (l *PgxLogger) Log(ctx context.Context, level etracelog.LogLevel, msg string, data map[string]any) {
	keyVals := make([]interface{}, 0, len(data))
	for k, v := range data {
		keyVals = append(keyVals, k, v)
	}

	switch level {
	case etracelog.LogLevelTrace, etracelog.LogLevelDebug:
		l.Logger.Debug(msg, keyVals...)
	case etracelog.LogLevelInfo:
		l.Logger.Info(msg, keyVals...)
	case etracelog.LogLevelWarn:
		l.Logger.Warn(msg, keyVals...)
	default:
		l.Logger.Error(msg, keyVals...)
	}
}
