package validate

import (
	"errors"
	"reflect"
	"strings"

	transformer "gitlab.com/reynaldio/rdio-galaxy/tag/component/transform/transformer"
)

var tagName = "transform"

// ref: https://stackoverflow.com/questions/6395076/using-reflect-how-do-you-set-the-value-of-a-struct-field
func Transform(obj interface{}) error {
	ref := reflect.ValueOf(obj)

	// if its a pointer, resolve its value
	if ref.Kind() == reflect.Ptr {
		ref = reflect.Indirect(ref)
	}

	if ref.Kind() == reflect.Interface {
		ref = ref.Elem()
	}

	// should double check we now have a struct (could still be anything)
	if ref.Kind() != reflect.Struct {
		return errors.New("Object must be of struct type")
	}

	fieldCount := ref.NumField()
	for i := 0; i < fieldCount; i++ {
		refField := ref.Type().Field(i)
		tagValue := refField.Tag.Get(tagName)
		// Skip if tag is not defined or ignored
		if len(tagValue) == 0 {
			continue
		}

		f := ref.Field(i)
		fType := f.Type().String()
		fName := refField.Name
		fValue := f.Interface()

		tagArgs := strings.Split(tagValue, ",")
		if fType == "string" {
			newValue, err := transformer.StringTransformer.Transform(fName, fType, fValue, tagArgs)
			if err != nil {
				return err
			}

			f.Set(reflect.ValueOf(newValue))
		}
	}

	return nil
}
