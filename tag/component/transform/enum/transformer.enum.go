package enum

const (
	TagArgUpper string = "upper"
	TagArgLower        = "lower"
	TagArgTitle        = "title"
	TagArgTrim         = "trim"
	TagArgEmail        = "email"
)

func IsValidTagArg(tagArg string) bool {
	if tagArg != TagArgUpper && tagArg != TagArgLower && tagArg != TagArgTitle && tagArg != TagArgTrim && tagArg != TagArgEmail {
		return false
	}

	return true
}
