package transformer

import (
	"strings"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	enum "gitlab.com/reynaldio/rdio-galaxy/tag/component/transform/enum"
	util "gitlab.com/reynaldio/rdio-galaxy/util"
)

type stringTransformer struct {
}

func (self *stringTransformer) Transform(fieldName string, fieldType string, fieldValue interface{}, tagArgs []string) (interface{}, error) {
	cFieldValue := fieldValue.(string)
	if len(cFieldValue) == 0 {
		return fieldValue, nil
	}

	for _, tagArg := range tagArgs {
		arg := strings.TrimSpace(tagArg)
		if len(arg) == 0 {
			continue
		}

		argElems := strings.Split(arg, "=")
		argName := argElems[0]

		if argName == enum.TagArgUpper {
			cFieldValue = strings.ToUpper(cFieldValue)
		} else if argName == enum.TagArgLower {
			cFieldValue = strings.ToLower(cFieldValue)
		} else if argName == enum.TagArgTitle {
			cFieldValue = cases.Title(language.English, cases.NoLower).String(cFieldValue)
		} else if argName == enum.TagArgTrim {
			cFieldValue = strings.TrimSpace(cFieldValue)
		} else if argName == enum.TagArgEmail {
			cFieldValue = util.EmailUtil.FormatEmail(cFieldValue)
		}
	}

	return cFieldValue, nil
}

var StringTransformer = &stringTransformer{}
