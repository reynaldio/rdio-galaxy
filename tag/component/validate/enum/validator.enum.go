package enum

const (
	TagArgRequired string = "required"
	TagArgMin             = "min"
	TagArgMax             = "max"
	TagArgEmail           = "email"
	TagArgPhone           = "phone"
)

func IsValidTagArg(tagArg string) bool {
	if tagArg != TagArgRequired && tagArg != TagArgMin && tagArg != TagArgMax && tagArg != TagArgEmail && tagArg != TagArgPhone {
		return false
	}

	return true
}
