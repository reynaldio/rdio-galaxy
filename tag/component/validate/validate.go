package validate

import (
	"context"
	"errors"
	"strings"

	model "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/model"
	validator "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/validator"
	tagmodel "gitlab.com/reynaldio/rdio-galaxy/tag/model"
	tagutil "gitlab.com/reynaldio/rdio-galaxy/tag/util"
)

var tagName = "validate"

type ValidationErrorHandler = func(ctx context.Context, validationResults []model.ValidatorResult) error

var validationErrorHandler ValidationErrorHandler

func SetValidationErrorHandler(handler ValidationErrorHandler) {
	validationErrorHandler = handler
}

func ValidateDef(tagDefs []tagmodel.TagDefinition) ([]model.ValidatorResult, error) {
	results := []model.ValidatorResult{}
	var err error

	for _, tagDef := range tagDefs {
		if tagDef.TagName != tagName || len(tagDef.TagValue) == 0 {
			continue
		}

		tagArgs := strings.Split(tagDef.TagValue, ",")
		if tagDef.FieldType == "string" {
			results, err = validator.StringValidator.Validate(results, tagDef.FieldName, tagDef.FieldType, tagDef.FieldValue, tagArgs)
			if err != nil {
				return nil, err
			}
		} else if tagDef.FieldType == "time.Time" {
			results, err = validator.TimeValidator.Validate(results, tagDef.FieldName, tagDef.FieldType, tagDef.FieldValue, tagArgs)
			if err != nil {
				return nil, err
			}
		} else if tagDef.FieldType == "uuid.UUID" {
			results, err = validator.UUIDValidator.Validate(results, tagDef.FieldName, tagDef.FieldType, tagDef.FieldValue, tagArgs)
			if err != nil {
				return nil, err
			}
		}
	}

	return results, nil
}

func Validate(obj interface{}) ([]model.ValidatorResult, error) {
	tagDefs, err := tagutil.TagUtil.GetTagDefinitionByName([]string{tagName}, obj)
	if err != nil {
		return nil, err
	}

	return ValidateDef(tagDefs)
}

// validate with handler
func ValidateDefExt(ctx context.Context, tagDefs []tagmodel.TagDefinition) error {
	if validationErrorHandler == nil {
		return errors.New("Validation error handler is not set")
	}

	valResults, err := ValidateDef(tagDefs)
	if err != nil {
		return err
	}

	return validationErrorHandler(ctx, valResults)
}

// validate with handler
func ValidateExt(ctx context.Context, obj interface{}) error {
	if validationErrorHandler == nil {
		return errors.New("Validation error handler is not set")
	}

	valResults, err := Validate(obj)
	if err != nil {
		return err
	}

	return validationErrorHandler(ctx, valResults)
}
