package validator

import (
	"strings"
	"time"

	enum "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/enum"
	model "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/model"
)

type timeValidator struct {
}

func (vl *timeValidator) Validate(results []model.ValidatorResult, fieldName string, fieldType string, fieldValue interface{}, tagArgs []string) ([]model.ValidatorResult, error) {
	newResults := append([]model.ValidatorResult{}, results...)
	cFieldValue := fieldValue.(time.Time)

	for _, tagArg := range tagArgs {
		arg := strings.TrimSpace(tagArg)
		if len(arg) == 0 {
			continue
		}

		argElems := strings.Split(arg, "=")
		argName := argElems[0]

		if argName == enum.TagArgRequired {
			if time.Time.IsZero(cFieldValue) {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   nil,
				}

				newResults = append(newResults, vr)
			}
		}
	}

	return newResults, nil
}

var TimeValidator = &timeValidator{}
