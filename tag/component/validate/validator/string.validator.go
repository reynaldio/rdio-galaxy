package validator

import (
	"errors"
	"strconv"
	"strings"

	enum "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/enum"
	model "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/model"
	"gitlab.com/reynaldio/rdio-galaxy/util"
)

type stringValidator struct {
}

func (vl *stringValidator) Validate(results []model.ValidatorResult, fieldName string, fieldType string, fieldValue interface{}, tagArgs []string) ([]model.ValidatorResult, error) {
	newResults := append([]model.ValidatorResult{}, results...)
	cFieldValue := fieldValue.(string)

	for _, tagArg := range tagArgs {
		arg := strings.TrimSpace(tagArg)
		if len(arg) == 0 {
			continue
		}

		argElems := strings.Split(arg, "=")
		argName := argElems[0]

		if argName == enum.TagArgRequired {
			if len(cFieldValue) == 0 {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   nil,
				}

				newResults = append(newResults, vr)
			}
		} else if argName == enum.TagArgMin {
			if len(argElems) < 2 {
				return nil, errors.New("Min tag must have value")
			}

			argValue, err := strconv.Atoi(argElems[1])
			if err != nil {
				return nil, err
			}

			if len(cFieldValue) < argValue {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   argValue,
				}

				newResults = append(newResults, vr)
			}
		} else if argName == enum.TagArgMax {
			if len(argElems) < 2 {
				return nil, errors.New("Max tag must have value")
			}

			argValue, err := strconv.Atoi(argElems[1])
			if err != nil {
				return nil, err
			}

			if len(cFieldValue) > argValue {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   argValue,
				}

				newResults = append(newResults, vr)
			}
		} else if argName == enum.TagArgEmail {
			if len(cFieldValue) > 0 && !util.EmailUtil.IsEmailValid(cFieldValue) {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   nil,
				}

				newResults = append(newResults, vr)
			}
		} else if argName == enum.TagArgPhone {
			if len(cFieldValue) > 0 && !util.PhoneUtil.IsPhoneNumberValid(cFieldValue) {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   nil,
				}

				newResults = append(newResults, vr)
			}
		}
	}

	return newResults, nil
}

var StringValidator = &stringValidator{}
