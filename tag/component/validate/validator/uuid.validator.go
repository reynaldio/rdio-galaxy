package validator

import (
	"strings"

	"github.com/google/uuid"

	enum "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/enum"
	model "gitlab.com/reynaldio/rdio-galaxy/tag/component/validate/model"
)

type uuidValidator struct {
}

func (vl *uuidValidator) Validate(results []model.ValidatorResult, fieldName string, fieldType string, fieldValue interface{}, tagArgs []string) ([]model.ValidatorResult, error) {
	newResults := append([]model.ValidatorResult{}, results...)
	cFieldValue := fieldValue.(uuid.UUID)

	for _, tagArg := range tagArgs {
		arg := strings.TrimSpace(tagArg)
		if len(arg) == 0 {
			continue
		}

		argElems := strings.Split(arg, "=")
		argName := argElems[0]

		if argName == enum.TagArgRequired {
			if cFieldValue == uuid.Nil {
				vr := model.ValidatorResult{
					FieldName:  fieldName,
					FieldValue: cFieldValue,
					FieldType:  fieldType,
					ArgName:    argName,
					ArgValue:   nil,
				}

				newResults = append(newResults, vr)
			}
		}
	}

	return newResults, nil
}

var UUIDValidator = &uuidValidator{}
