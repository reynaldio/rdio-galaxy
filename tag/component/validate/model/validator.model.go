package model

type ValidatorResult struct {
	FieldName  string
	FieldValue interface{}
	FieldType  string
	ArgName    string
	ArgValue   interface{}
}
