package model

type TagDefinition struct {
	TagName    string
	TagValue   string
	FieldName  string
	FieldType  string
	FieldValue interface{}
}
