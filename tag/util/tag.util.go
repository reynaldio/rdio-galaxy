package util

import (
	"errors"
	"reflect"

	model "gitlab.com/reynaldio/rdio-galaxy/tag/model"
)

type tagUtil struct {
}

// https://www.golangprograms.com/regular-expression-to-validate-phone-number.html
func (self *tagUtil) GetTagDefinitionByName(tagNames []string, obj interface{}) ([]model.TagDefinition, error) {
	results := []model.TagDefinition{}

	ref := reflect.ValueOf(obj)

	// if its a pointer, resolve its value
	if ref.Kind() == reflect.Ptr {
		ref = reflect.Indirect(ref)
	}

	if ref.Kind() == reflect.Interface {
		ref = ref.Elem()
	}

	// should double check we now have a struct (could still be anything)
	if ref.Kind() != reflect.Struct {
		return nil, errors.New("Object must be of struct type")
	}

	fieldCount := ref.NumField()
	for i := 0; i < fieldCount; i++ {
		for _, tagName := range tagNames {
			refField := ref.Type().Field(i)
			tagValue := refField.Tag.Get(tagName)
			// Skip if tag is not defined or ignored
			if len(tagValue) == 0 {
				continue
			}

			f := ref.Field(i)
			fType := f.Type().String()
			fName := refField.Name
			fValue := f.Interface()

			tagDefinition := model.TagDefinition{
				TagName:    tagName,
				TagValue:   tagValue,
				FieldName:  fName,
				FieldType:  fType,
				FieldValue: fValue,
			}

			results = append(results, tagDefinition)
		}
	}

	return results, nil
}

var TagUtil = &tagUtil{}
