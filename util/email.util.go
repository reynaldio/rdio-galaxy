package util

import (
	"net/mail"
	"strings"
)

type emailUtil struct {
}

func (self *emailUtil) IsEmailValid(email string) bool {
	s := strings.TrimSpace(email)
	if len(s) <= 0 {
		return false
	}

	_, err := mail.ParseAddress(s)
	if err != nil {
		return false
	}

	return true
}

func (self *emailUtil) FormatEmail(email string) string {
	s := strings.ToLower(strings.TrimSpace(email))
	if len(s) <= 0 {
		return ""
	}

	m, err := mail.ParseAddress(s)
	if err != nil {
		return s
	}

	return m.Address
}

// check if email valid and format to standard format
func (self *emailUtil) ValidateAndFormatEmail(email string) (string, bool) {
	s := strings.ToLower(strings.TrimSpace(email))
	if len(s) <= 0 {
		return "", false
	}

	m, err := mail.ParseAddress(s)
	if err != nil {
		return m.Address, false
	}

	return s, true
}

var EmailUtil = &emailUtil{}
