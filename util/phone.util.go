package util

import (
	"strings"

	"github.com/nyaruka/phonenumbers"

	model "gitlab.com/reynaldio/rdio-galaxy/model"
)

type phoneUtil struct {
}

func (self *phoneUtil) IsPhoneNumberValid(phoneNumber string) bool {
	s := strings.ToLower(strings.TrimSpace(phoneNumber))
	if len(s) <= 0 {
		return false
	}

	pn, err := phonenumbers.Parse(s, "ID")
	if err != nil {
		return false
	}

	return phonenumbers.IsValidNumber(pn)
}

// check if phoneNumber valid and format to standard format
func (self *phoneUtil) ValidateAndFormatPhoneNumber(countryId string, phoneNumber string) (string, bool) {
	s := strings.ToLower(strings.TrimSpace(phoneNumber))
	if len(s) <= 0 {
		return "", false
	}

	if !self.IsPhoneNumberValid(s) {
		return "", false
	}

	pn, err := phonenumbers.Parse(s, strings.ToUpper(countryId))
	if err != nil {
		return "", false
	}

	return phonenumbers.Format(pn, phonenumbers.INTERNATIONAL), true
}

func (self *phoneUtil) ExtractPhoneNumberInfo(phoneNumber string) (*model.PhoneNumber, error) {
	s := strings.ToLower(strings.TrimSpace(phoneNumber))
	if len(s) <= 0 {
		return nil, nil
	}

	pn, err := phonenumbers.Parse(s, "")
	if err != nil {
		return nil, err
	}

	if !phonenumbers.IsValidNumber(pn) {
		return nil, nil
	}

	result := &model.PhoneNumber{
		PhoneCountryId:   phonenumbers.GetRegionCodeForNumber(pn),
		PhoneCountryCode: *pn.CountryCode,
		PhoneNumber:      phonenumbers.Format(pn, phonenumbers.NATIONAL),
		FullPhoneNumber:  phonenumbers.Format(pn, phonenumbers.INTERNATIONAL),
	}

	return result, nil
}

var PhoneUtil = &phoneUtil{}
