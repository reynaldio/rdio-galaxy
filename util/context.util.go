package util

import (
	"context"
)

type contextUtil struct {
}

func (self *contextUtil) GetStringValue(ctx context.Context, key string) string {
	cValue := ctx.Value(key)
	if cValue != nil {
		return cValue.(string)
	}

	return ""
}

var ContextUtil = &contextUtil{}
