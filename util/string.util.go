package util

import (
	"reflect"
	"regexp"
	"strings"
)

var alphanumericRegex = regexp.MustCompile(`[^a-zA-Z0-9]+`)

type stringUtil struct {
}

func (self *stringUtil) IsEmptyString(s string) bool {
	if len(strings.TrimSpace(s)) == 0 {
		return true
	}

	return false
}

func (self *stringUtil) RemoveNonAlphaNumeric(s string) string {
	result := alphanumericRegex.ReplaceAllString(s, "")
	return result
}

func (self *stringUtil) FindNullStringFieldFromStruct(o interface{}, fieldNames ...string) string {
	v := reflect.ValueOf(o)
	for _, fn := range fieldNames {
		f := v.FieldByName(fn)
		if len(f.String()) <= 0 {
			return fn
		}
	}

	return ""
}

func (self *stringUtil) FindExceedMaxLengthStringFieldFromStruct(o interface{}, fieldNameAndMaxLength map[string]int) (string, int) {
	v := reflect.ValueOf(o)
	for fn, maxLength := range fieldNameAndMaxLength {
		f := v.FieldByName(fn)
		if len(f.String()) > maxLength {
			return fn, maxLength
		}
	}

	return "", 0
}

var StringUtil = &stringUtil{}
